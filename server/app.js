var express = require('express');
var path = require('path');
var app = express();

// Define the port to run on
var definedPort = (process.env.PORT || process.env.VCAP_APP_PORT || 8888);
app.set('host', '0.0.0.0');
app.set('port', definedPort);

app.use(express.static(path.join(__dirname, 'app')));

var http = require('http').Server(app);

http.listen(3000, "0.0.0.0");
// Listen for requests
/*var server = app.listen(app.get('port'), app.get('host'), function() {
	var host = server.address().host;
	var port = server.address().port;
	console.log('Server started at ' + host + ':' + port);
});*/