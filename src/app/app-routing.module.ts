import { HomeComponent } from './components/home/home.component';
import { MapComponent } from './components/map/map.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
                          {path: 'map', component: MapComponent},
                          {path: 'home', component: HomeComponent},
                          {path: '**', redirectTo: '/map', pathMatch: 'full'}
                        ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
