import { CoOrdinate } from '../../model/co-ordinate';
import { Poi } from '../../model/poi';
import { Component, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { Form, FormBuilder, Validators, FormGroup } from '@angular/forms';
import * as d3 from 'd3';
import { stringify } from 'querystring';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, AfterViewInit {
  el: ElementRef;
  poiList: Poi[] = [];
  geoForm: FormGroup;
  lat: number;
  long: number;
  svg: any;
  projection: any;

  constructor(private http: Http,el: ElementRef, private fb: FormBuilder) {
    this.el = el;
    this.geoForm = fb.group({
        'poiName': [null, Validators.required],
        'address': [null, Validators.required],
        'city': ['Salt Lake', Validators.required],
        'state': ['West Bengal', Validators.required],
        'zipCode': [null, Validators.required],
        'openTime': [null, Validators.required],
        'closeTime': [null, Validators.required],
        'noOfWorkingHours': [null, Validators.required],
        'lat': [22.58832326772921, Validators.required],
        'long': [88.40703655040988, Validators.required]
  });

    this.lat = 22.58832326772921;
    this.long = 88.40703655040988;
  }


  ngAfterViewInit() {
    const hostElem = this.el.nativeElement;
    console.log(hostElem.children);
    console.log(hostElem.parentNode);
    let self = this;
    let width = 1060;
    let height = 780;
  //  let width = $('svg').parent().width();
//    let height = $('svg').parent().height();
    // alert(d3.select('.map').attr('width'));
     this.svg = d3.select('.map')
                      .append('svg')
                      .attr('width', width)
    .attr('height', height);
//    .attr('viewBox','0 0 '+Math.min(width,height)+' '+Math.min(width,height))
//    .attr('preserveAspectRatio','xMinYMin')
//    .append("g")
//    .attr("transform", "translate(" + Math.min(width,height) / 2 + "," + Math.min(width,height) / 2 + ")");
    // svg.attr('height', height);
    // svg.attr('width', width);

    this.projection = d3.geoMercator()
      .scale(400000)
      // .scale(100)
      .center([88.40703655040988, 22.58832326772921]);

    let path = d3.geoPath()
      .projection(self.projection);



    let url = '/assets/mapdata/bidhan_landuse_final.json';
    //let url = '/assets/mapdata/bidhan_landuse_final.json';
    d3.json(url, function(err, geojson) {
      self.svg.append('path')
        .attr('d', path(geojson))
        .attr('fill', 'grey');
    });

    d3.select('svg').on('click', function() {
      let longLat = self.projection.invert(d3.mouse(this));
      self.setLongLat(longLat);

    });

  }

  setLongLat(longLat) {
    //console.log(this);
    //console.log(longLat);
       this.long = longLat[0];
       this.lat = longLat[1];
  }


  ngOnInit() {
  }

  addLocation(post) {
      
      this.savePoi(this.genrateParamList(post));

     // Define the div for the tooltip
     var div = d3.select('body').append('div')
        .attr('class', 'tooltip')
        .style('opacity', 0);

     let self = this;
     let coOrd: CoOrdinate = new CoOrdinate(post.long, post.lat);

     this.poiList.push(new Poi(post.poiName, post.address, coOrd));
     console.log(this.svg.selectAll('circle'));
     let dataList = [];
     this.poiList.forEach(function(d) {
       dataList.push({poiName: post.poiName, address: post.address, openTime: post.openTime,
          closeTime: post.closeTime, noOfWorkingHours: post.noOfWorkingHours, coOrd: [post.long, post.lat]});
     });
     let data = this.svg.selectAll('circle')
        .data(dataList);

     data.enter()
        .append('circle')
        .attr('cx', function (d) { console.log('=' + d + '=' + self.projection(d.coOrd)); return self.projection(d.coOrd)[0]; })
        .attr('cy', function (d) { console.log('=' + JSON.stringify(d)); return self.projection(d.coOrd)[1]; })
        .attr('r', '8px')
        .attr('fill', 'red')
        .on('mouseover', function(d) {
            div.transition()
                .duration(200)
                .style('opacity', .9);
            div .html(self.generateHtmlToolTip(d))
                .style('left', (d3.event.pageX) + 'px')
                .style('top', (d3.event.pageY - 28) + 'px');
            })
        .on('mouseout', function(d) {
            div.transition()
                .duration(500)
                .style('opacity', 0);
        });

    return true;

  }

  generateHtmlToolTip(data) {
    let html = '<b><u>' + data.poiName + '</u></b><br/>';
    html += '<div style="text-align:left"><b>Address</b>:' + data.address + '<br/></div>';
    html += '<div style="text-align:left"><b>Open Time</b>:' + data.openTime + '<br/></div>';
    html += '<div style="text-align:left"><b>Close Time</b>:' + data.closeTime + '<br/></div>';
    html += '<div style="text-align:left"><b>No. of working hours</b>:' + data.noOfWorkingHours + '<br/></div>';
    return html;
  }

genrateParamList(post:any){
    
    let paramList = new URLSearchParams();
    paramList.append('app_medical_shop_name',post.poiName);
    paramList.append('app_medical_shop_addr1',post.address);
    paramList.append('app_medical_shop_addr2','');
    paramList.append('app_medical_shop_city',post.city);
    paramList.append('app_medical_shop_state',post.state);
    paramList.append('app_medical_shop_postalcode',post.zipCode);
    paramList.append('latitude',post.lat);
    paramList.append('longitude',post.long);
    paramList.append('opening_time',post.openTime);
    paramList.append('closing_time',post.closeTime);
    paramList.append('working_days','7');
    paramList.append('working_hours',post.noOfWorkingHours);
    
    
    
    return paramList;

}

savePoi(params:URLSearchParams){
    let options = new RequestOptions({ headers: new Headers(), params: params });
    return this.http.post('http://www.ingminds.com/projects/map_admin/medical_shop',params.toString())

    .subscribe((res)=>{return res},(e)=>alert(e));
}
}
