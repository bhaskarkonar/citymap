import { CoOrdinate } from './co-ordinate';
export class Poi {
  constructor(
    public poiName: string,
    public address: string,
    public poiCordonate: CoOrdinate
  ) {}
}
